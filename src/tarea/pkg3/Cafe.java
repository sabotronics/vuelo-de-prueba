package tarea.pkg3;

import java.util.*;

public class Cafe {
    public static void main(String[] args) {
        amistades(args);
    }

    public static void amistades(String[] args) {
        List <Integer> grone = new ArrayList<>();
        List <Integer> chele = new ArrayList<>();
        List <Integer> sugar = new ArrayList<>();
        List <Integer> bitter = new ArrayList<>();
        List <Integer> sweet = new ArrayList<>();
        List <Integer> salt = new ArrayList<>();
        int i = 1;
        for (i = 1; i <= 3; i++) {
            System.out.println("\nPedido #"+i+"\nNombre del Invitado: ");
            Scanner var = new Scanner(System.in);
            String nombre = var.nextLine();
            System.out.println("\nDigite una opción:\n"
                    + "1- Negro \n2- Con leche ");
            Scanner var0 = new Scanner(System.in);
            int cafe = var0.nextInt();
            System.out.println("\nDigite una opción:\n"
                    + "1- Con azúcar \n2- Amargo ");
            Scanner var1 = new Scanner(System.in);
            int azucar = var1.nextInt();
            System.out.println("\nDigite una opción:\n"
                    + "1- Pan dulce \n2- Pan salado");
            Scanner var2 = new Scanner(System.in);
            int pan = var2.nextInt();
            System.out.println("\nInvitado: "+nombre+"\n***Pedido***" );
            if (cafe == 1){
                System.out.println("Café: Negro");
                grone.add(1);
            } if (cafe == 2){
                System.out.println("Café: Con Leche");
                chele.add(1);
            } if (azucar == 1){
                System.out.println("Con azúcar");
                sugar.add(1);
            } if (azucar == 2){
                System.out.println("Sin azúcar");
                bitter.add(1);
            } if (pan == 1){
                System.out.println("Pan: Dulce");
                sweet.add(1);
            } if (pan == 2){
                System.out.println("Pan: Salado\n");
                salt.add(1);
            }
        }
        int tgrone = grone.size();
        int tchele = chele.size();
        int tsugar = sugar.size();
        int tbitter = bitter.size();
        int tsweet = sweet.size();
        int tsalt = salt.size();
        
        System.out.print("\nTotal de Pedidos:\nCafé negro: "+tgrone
                + "\nCafé con leche: "+tchele+"\nCafé con azúcar: "+tsugar
                        + "\nCafé amargo: "+tbitter+"\nPan dulce: "+tsweet
                                + "\nPan salado: "+tsalt);
        System.out.print("\nSe han realizado todos los pedidos.");
    }
}
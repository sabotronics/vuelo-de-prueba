package tarea.pkg3;

import java.util.*;

public class Sillas {

    public static void main(String[] args) {
        tienda(args);
    }

    public static void tienda(String[] args) {
        System.out.println("\nBienvenidos al Centro Plástico Chino\n"
                + "\nIndique la cantidad de sillas:");
        Scanner var = new Scanner(System.in);
        int cant = var.nextInt();
        System.out.print("\nSi las sillas son blancas, digite '1'\n"
                + "de otro modo, cualquier otro número: \n");
        Scanner var0 = new Scanner(System.in);
        int color = var0.nextInt();
        if (cant < 4) {
            int sub = 15000 * cant;
            System.out.println("\nCantidad de sillas: " + cant + " x ¢15000\n"
                    + "Subtotal: ¢" + sub);
            if (color != 1) {
                System.out.println("\nImpuesto por color: ¢" + sub * 0.05);
                System.out.println("\nTotal: ¢" + (sub + (sub * 0.05)));
                System.out.println("\nMuchas Gracias!");
            } else {
                System.out.println("\nTotal: ¢" + sub);
                System.out.println("\nMuchas Gracias!");
            }

        } else if (cant > 3 && cant < 9) {
            int sub1 = 11000 * cant;
            System.out.print("\nCantidad de sillas: " + cant + " x ¢11000\n"
                    + "Subtotal: ¢" + sub1);
            if (color != 1) {
                System.out.println("\nImpuesto por color: ¢" + sub1 * 0.05);
                System.out.println("\nTotal: ¢" + (sub1 + (sub1 * 0.05)));
                System.out.println("\nMuchas Gracias!");
            } else {
                System.out.println("\nTotal: ¢" + sub1);
                System.out.println("\nMuchas Gracias!");
            }

        } else if (cant > 8) {
            int sub2 = 10000 * cant;
            System.out.println("\nCantidad de sillas: " + cant + " x ¢10000\n"
                    + "Subtotal: ¢" + sub2);
            if (color != 1) {
                System.out.println("\nImpuesto por color: ¢" + sub2 * 0.05);
                System.out.println("\nTotal: ¢" + (sub2 + (sub2 * 0.05)));
                System.out.println("\nMuchas Gracias!");
            } else {
                System.out.println("\nTotal: ¢" + sub2);
                System.out.println("\nMuchas Gracias!");
            }

        }

    }
}
